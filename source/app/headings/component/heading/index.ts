import './heading.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { State, Action } from 'vuex-class';

import TagComponent from '../../../core/components/tag';
import { ITag } from '../../../filters/interfaces';
import { ICategory } from '../../interfaces';

@Component({
  template: require('./template.pug'),
  components: {
    'v-tag': TagComponent
  },
})
export default class HeadingComponent extends Vue {

  @State(state => state.filters.tags) tags;

  @Action('A_ADD_TAG') addTag;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggle;

  @Prop({ default: <ICategory>{
    articles_count: 0,
    description: '',
    id: 0,
    name: '',
    image: '/assets/images/post_tmp.png'}
  })
  category: ICategory;

  get isInTagList(): boolean {
    if (this.tags[this.category.id] !== undefined) {
      return true;
    }
    return false;
  }

  get fullText(): string {
    return `${this.category.name} ${this.category.articles_count}`;
  }

  private addTagToList() {
    let newTag = <ITag<{name: string, type: string, country_geoid: number}>>{
      [this.category.id]: {
        name: this.category.name,
        type: 'category',
        country_geoid: 2017370,
      }
    };

    this.addTag(newTag);
    this.toggle(false);
  }
}
