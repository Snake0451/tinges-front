export interface ICategory {

    articles_count: number;

    description: string;

    id: number;

    name: string;
}
