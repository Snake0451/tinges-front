import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import * as TYPES from './types';
import { HTTP } from '../../core/http';

@module
export default class HeadingsStore extends TypedStore {

  public categories: Array<any> = [];

  @action
  [TYPES.A_FETCH_CATEGORIES]() {
    HTTP.get(`categories`)
      .then(response => {
        this.commit(TYPES.M_SET_CATEGORIES, response.data);
      });
  }

  @mutation
  [TYPES.M_SET_CATEGORIES](data: Array<any>) {
    this.categories = data;
  }
}

