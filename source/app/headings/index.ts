import Vue from 'vue';
import Component from 'vue-class-component';
import { Action, State } from 'vuex-class';

import HeadingComponent from './component/heading/';

@Component({
  template: require('./template.pug'),
  components: {
    'v-heading': HeadingComponent
  },
})
export default class HeadingsComponent extends Vue {

  @State(state => state.headings.categories) categories: Array<any>;

  @Action('A_FETCH_CATEGORIES') fetchCategories;

  created() {
    this.fetchCategories();
  }
}
