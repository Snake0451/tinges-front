import './sharing.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

import { IArticle } from '../playList/intefaces';

@Component({
  template: require('./template.pug')
})
export default class SharingComponent extends Vue {

  @Prop({ default: () => <IArticle>{
    id: 0,
    image: '/assets/images/map_img.jpg',
    location_geoid: 0,
    short_content: '',
    tags: [],
    title: '',
    url: '',
    created_at: ''
  }})
  data: IArticle;

  private openSharePopup(url: string) {
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
  }

  private shareToVkontakte(event): void {
    event.preventDefault();
    let shareString = `http://vkontakte.ru/share.php?url=${encodeURIComponent(this.data.url)}&title=${encodeURIComponent(this.data.title)}&description=${encodeURIComponent(this.data.short_content)}&noparse=true`;
    this.openSharePopup(shareString);
  }

  private shareToFacebook(event): void {
    event.preventDefault();
    let shareString = `http://www.facebook.com/sharer.php?s=100&p[title]=${encodeURIComponent(this.data.title)}&p[summary]=${encodeURIComponent(this.data.short_content)}&p[url]=${encodeURIComponent(this.data.url)}`;
    this.openSharePopup(shareString);
  }

  private shareToTwitter(event): void {
    event.preventDefault();
    let shareString = `http://twitter.com/share?text=${encodeURIComponent(this.data.title)}&url=${encodeURIComponent(this.data.url)}`;
    this.openSharePopup(shareString);
  }

  private shareToOdnoklassniki(event): void {
    event.preventDefault();
    let shareString = `http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st.comments=${encodeURIComponent(this.data.title)}&st._surl=${encodeURIComponent(this.data.url)}`;
    this.openSharePopup(shareString);
  }
}
