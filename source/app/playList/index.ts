import './playList.scss';

import Vue from 'vue';
import $ from 'jquery';
import { debounce } from 'lodash';
import Component from 'vue-class-component';
import { State, Action, Getter } from 'vuex-class';
import { Watch } from 'vue-property-decorator';

import { IMapBounds } from '../gmap/interfaces';
import { IArticle } from './intefaces';
import { IPagination } from '../core/interfaces';

import PostComponent from './components/post';
import videoEvent from '../core/components/videoEvent';
import videoTime from '../core/components/videoTime';
import videoEmotions from '../core/components/videoEmotions';

@Component({
  template: require('./template.pug'),
  components: {
    'v-post': PostComponent,
    'v-videoEvent': videoEvent,
    'v-videoEmotions': videoEmotions,
    'v-videoTime': videoTime
  }
})
export default class playListComponent extends Vue {

  @State(state => state.playList.articles) articles: IPagination;

  @State(state => state.layout.isSidebarAtMaxScroll) isSidebarAtMaxScroll: boolean;

  @Getter mapBounds;

  @Action('A_FETCH_ARTICLES') fetchArticles;

  @Action('A_TOGGLE_SIDEBAR_MAX_SCROLL') toggleSidebarMaxScroll;

  private isLoadingMore: boolean = false;

  private clearArticles: boolean = false;

  private loadedArticles: Array<IArticle> = [];

  @Watch('mapBounds', { deep: true })
  onBoundsChanged(value) {
    if (Object.keys(value).length !== 0 && value.constructor === Object) {
      this.clearArticles = true;
      this.fetchArticles({
        bounds: value
      }).then(() => {
        this.loadedArticles.push.apply(this.loadedArticles, this.articles.data);
      });
    }
  }

  @Watch('isSidebarAtMaxScroll')
  onMaxScrollReached(value) {
    if (value && !this.isLoadingMore) {
      this.isLoadingMore = value;
      this.fetchArticles({bounds: this.mapBounds, page: this.articles.current_page + 1}).then(() => {
        this.toggleSidebarMaxScroll();
      });
    }
    else if (value === false) {
      this.isLoadingMore = value;
    }
  }

  @Watch('articles')
  onArticlesPageLoaded(value) {
    if (this.clearArticles) {
      this.loadedArticles = [];
      this.clearArticles = false;
    }
    this.loadedArticles.push.apply(this.loadedArticles, value.data);
    this.loadedArticles = this.deduplicate();
  }

  mounted() {
    if (Object.keys(this.articles).length === 0 && this.articles.constructor === Object) {
      this.clearArticles = true;
      this.fetchArticles({
        bounds: this.mapBounds
      });
    }
    else {
      this.loadedArticles = [];
      this.loadedArticles.push.apply(this.loadedArticles, this.articles.data);
    }

    const scrollbarContainer = $(this.$refs['scrollbar']).find('.playList');
    scrollbarContainer.on('scroll', debounce(this.checkMaxScroll, 150));
  }

  checkMaxScroll() {
    const vueBar = this.$refs['scrollbar'];
    const scrollbarContainer = $(vueBar).find('.playList');
    const scrollbar = $(vueBar).find('.vb-dragger');
    const scrollbarTop = scrollbar.position().top;
    const scrollbarHeight = scrollbar.height();

    if (scrollbarTop + scrollbarHeight >= scrollbarContainer.height() - 100) {
      this.handleMaxScroll();
    }
  }

  handleMaxScroll() {
    if (!this.isSidebarAtMaxScroll) {
      this.toggleSidebarMaxScroll();
    }
  }

  deduplicate() {
    const hashTable = {};

    return this.loadedArticles.filter((el) => {
      const key = JSON.stringify(el);
      const match = Boolean(hashTable[key]);

      return (match ? false : hashTable[key] = true);
    });
  }

  get isEmptyArticles() {
    return this.loadedArticles.length === 0;
  }
}
