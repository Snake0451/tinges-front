import './post.scss';
import * as moment from 'moment';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Prop } from 'vue-property-decorator';

import TagComponent from '../../../core/components/tag';
import HotLabelComponent from '../../../core/components/hot-label';
import SpoilerComponent from '../../../core/components/spoiler';
import { IMapLocation } from '../../../gmap/interfaces';
import { ITag } from '../../../filters/interfaces';

import '../../../core/directives/date-format';
import { IArticle } from '../../intefaces';

@Component({
  template: require('./template.pug'),
  components: {
    'v-tag': TagComponent,
    'v-hot-label': HotLabelComponent,
    'v-spoiler': SpoilerComponent
  }
})
export default class PostComponent extends Vue {

  @Prop({default: () => {
    return <IArticle>{
      created_at: '',
      id: 0,
      image: '/assets/images/post_tmp.png',
      location_geoid: 0,
      short_content: '',
      tags: [],
      title: '',
      url: '',
    };
  }})
  data: IArticle;

  @State(state => state.map.locations) locations: Array<IMapLocation>;

  @State(state => state.layout.isContentWindowOpened) isContentWindowOpened: boolean;

  @Action('A_ADD_TAG') addTag;

  @Action('A_FETCH_ARTICLE') openArticle;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggleContentWindow;

  private getArticle(id: number) {
    this.openArticle(id).then(response => {
      if (!this.isContentWindowOpened) {
        this.toggleContentWindow();
      }
    });
  }

  private filterByLocation(cityName) {
    let newTag = <ITag<{name: string, type: string}>>{
      [this.data.location_geoid]: {
        name: cityName,
        type: 'city'
      }
    };

    this.addTag(newTag);
  }

  get articleCity() {
    const city = this.locations.filter(loc => loc.location_geoid === this.data.location_geoid)[0];
    return city ? city.name_ru : 'Город';
  }

  get previewImage() {
    if (typeof this.data.previews !== 'undefined') {
      return 'https://vmap.atwinta.ru/' + this.data.previews['380x180'];
    }
    else {
      return '';
    }
  }

  get isNew(): boolean {
    let itemDate = moment(this.data.created_at).format('YYYY-MM-DD');
    let dateLimit = moment().subtract(7, 'd').format('YYYY-MM-DD');

    if (itemDate > dateLimit) {
      return true;
    }
    return false;
  }
}
