export interface IArticle {
  category_id?: number;

  content?: string;

  id: number;

  image: string;

  item_id?: number;

  location_geoid: number;

  previews: Object;

  short_content: string;

  tags: Array<string>;

  title: string;

  url: string;

  created_at: string;

  updated_at?: string;
}
