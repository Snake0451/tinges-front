import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import * as TYPES from './types';
import { HTTP } from '../../core/http';

import { IMapBounds } from '../../gmap/interfaces';
import { IArticle } from '../intefaces';
import { IPagination } from '../../core/interfaces';

interface IArticlesFetchParams {
  bounds: IMapBounds;

  page?: number;
}

@module
export default class PlayListStore extends TypedStore {

  public articles: IPagination = <IPagination>{};

  private articlesForlocation: IPagination = <IPagination>{};

  public article: IArticle = <IArticle>{
    category_id: 0,
    content: '',
    id: 0,
    image: '',
    item_id: 0,
    location_geoid: 0,
    short_content: '',
    tags: [],
    title: '',
    url: '',
    created_at: '',
    updated_at: ''
  };

  @action
  [TYPES.A_FETCH_ARTICLES](data: IArticlesFetchParams) {
    let getData = {...data.bounds, page: data.page};

    HTTP.get(`articles`, { params: getData })
      .then(response => {
        this.commit(TYPES.M_SET_ARTICLES, response.data);
      })
      .catch(error => {

      });
  }

  @action
  [TYPES.A_FETCH_ARTICLES_FOR_LOCATION](data: {locationId: number, page?: number}) {
    return new Promise((resolve, reject) => {
      HTTP.get(`locations/${data.locationId}/articles`, {
        params: {
          page: data.page
        }
      })
      .then(response => {
        this.commit(TYPES.M_SET_ARTICLES_FOR_LOCATION, response.data);
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
    });
  }

  @action
  [TYPES.A_FETCH_ARTICLE](articleId: number) {
    HTTP.get(`articles/${articleId}`)
      .then(response => {
        this.commit(TYPES.M_SET_ARTICLE, response.data);
      });
  }

  @mutation
  [TYPES.M_SET_ARTICLES](data: IPagination) {
    this.articles = data;
  }

  @mutation
  [TYPES.M_SET_ARTICLES_FOR_LOCATION](data: IPagination) {
    this.articlesForlocation = data;
  }

  @mutation
  [TYPES.M_SET_ARTICLE](data: IArticle) {
    this.article = data;
  }

}

