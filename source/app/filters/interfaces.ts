export interface ITag<T> {

  [id: number]: T

}
