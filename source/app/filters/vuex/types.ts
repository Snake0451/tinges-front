export const A_ADD_TAG = 'A_ADD_TAG';
export const M_UPDATE_TAGS = 'M_UPDATE_TAGS';

export const A_REMOVE_TAG = 'A_REMOVE_TAG';
export const M_REMOVE_TAG = 'M_REMOVE_TAG';

export const A_SET_SEARCH_QUERY = 'A_SET_SEARCH_QUERY';
export const M_CHANGE_SEARCH_QUERY = 'M_CHANGE_SEARCH_QUERY';
