import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import * as TYPES from './types';

import { ITag } from '../interfaces';

@module
export default class FiltersStore extends TypedStore {

  /** @type {name: string, type: string}
  * Used to render tags list in sidebar
  */
  public tags: ITag<{name: string, type: string, country_geoid: number}> = {};

  /** @type {string}
  * Used to filter locations on map.
  */
  public preparedTags: ITag<{name: string, type: string, country_geoid: number}> = {};

  /** @type {string}
  * Search query to find countries and cities
  */
  public searchQuery: string = '';

  @action
  [TYPES.A_ADD_TAG](data: ITag<{name: string, type: string, country_geoid: number}>) {
    this.commit(TYPES.M_UPDATE_TAGS, data);
  }

  @action
  [TYPES.A_REMOVE_TAG](index: number) {
    this.commit(TYPES.M_REMOVE_TAG, index);
  }

  @action
  [TYPES.A_SET_SEARCH_QUERY](query: string) {
    this.commit(TYPES.M_CHANGE_SEARCH_QUERY, query);
  }

  @mutation
  [TYPES.M_UPDATE_TAGS](data: ITag<{name: string, type: string, country_geoid: number}>) {
    let key = Object.keys(data)[0];
    this.tags = {...this.tags,
      [key]: data[key]
    };

    // Add tag to prepared set. And remove parent tag if need
    let newPreparedTags;

    if (data[key].type === 'city') {
      if (this.preparedTags.hasOwnProperty(data[key].country_geoid)) {
          delete this.preparedTags[data[key].country_geoid];
      }
    }

    newPreparedTags = {...this.preparedTags,
      [key]: data[key]
    };

    this.preparedTags = newPreparedTags;
  }

  @mutation
  [TYPES.M_REMOVE_TAG](index: number) {
    // Delete from renders list
    delete this.tags[index];
    let newTags = {...this.tags};
    this.tags = newTags;

    // TODO: optimize this
    if (this.preparedTags.hasOwnProperty(index)) {
      let newPreparedTags;
      let isOtherCitiesForCountry = false;
      let tagForDeleting = this.preparedTags[index];

      delete this.preparedTags[index];

      if (tagForDeleting.type === 'city') {

        for (let tag in this.preparedTags) {
          if (this.preparedTags[tag].country_geoid === tagForDeleting.country_geoid) {
            isOtherCitiesForCountry = true;

            break;
          }

        }

        if (!isOtherCitiesForCountry && typeof this.tags[tagForDeleting.country_geoid] !== 'undefined') {

          newPreparedTags = {...this.preparedTags,
            [tagForDeleting.country_geoid]: {
              name: this.tags[tagForDeleting.country_geoid].name,
              type: 'country',
              country_geoid: tagForDeleting.country_geoid
            }
          };
        }
        else {
          newPreparedTags = {...this.preparedTags};
        }
      }
      else {
        newPreparedTags = {...this.preparedTags};
      }

      this.preparedTags = newPreparedTags;
    }
  }

  @mutation
  [TYPES.M_CHANGE_SEARCH_QUERY](query: string) {
    this.searchQuery = query;
  }
}

