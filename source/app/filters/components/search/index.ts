import './search.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Watch } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class SearchComponent extends Vue {

  @Action('A_SET_SEARCH_QUERY') setSearchQuery;

  private query: string = '';

  @Watch('query')
  onQueryChange(value) {
    this.setSearchQuery(value);
  }

}
