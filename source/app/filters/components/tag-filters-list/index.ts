import './tag-filters-list.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';

import { ITag } from '../../interfaces';
import TagComponent from '../../../core/components/tag';

@Component({
  template: require('./template.pug'),
  components: {
    'v-tag': TagComponent
  }
})
export default class TagFiltersListComponent extends Vue {

  @State(state => state.filters.tags) tags: ITag<string>;

  @Action('A_REMOVE_TAG') removeTag;

  private removeTagFromList(index: number) {
    this.removeTag(index);
  }

}
