import Vue from 'vue';
import * as moment from 'moment';
import {isCombinedModifierFlagSet} from 'tslint';

moment.locale('Ru-ru');

Vue.directive('date-format', {
  inserted: function(element) {
    element.innerHTML = moment(element.innerHTML).format('DD MMMM YYYY, H:mm').toString();
  },
  update: function(element, binding) {
    if (binding.value !== binding.oldValue) {
      element.innerHTML = moment(binding.value).format('DD MMMM YYYY, H:mm').toString();
    }
  }
});
