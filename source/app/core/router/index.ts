import VueRouter from 'vue-router';

import PlayListComponent from '../../playList';
import HeadingsComponent from '../../headings';
import GeographyComponent from '../../geography';

export default new VueRouter({
  mode: 'history',
  routes: <any>[{
      name: 'playList',
      title: 'Последние видео',
      path: '/',
      component: PlayListComponent
    }, {
      name: 'geography',
      title: 'География',
      path: '/geography',
      component: GeographyComponent
    }, {
      name: 'headings',
      title: 'Рубрики',
      path: '/headings',
      component: HeadingsComponent
    }]
});
