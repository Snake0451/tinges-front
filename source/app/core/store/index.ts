import TypedStore from './typedstore';
import { module } from 'vuex-ts-decorators';

import LayoutStore from '../../layout/vuex';
import MapStore from '../../gmap/vuex';
import PlayListStore from '../../playList/vuex';
import GeographyStore from '../../geography/vuex';
import FiltersStore from '../../filters/vuex';
import HeadingsStore from '../../headings/vuex';

@module({
  store: true,
  modules: {
    layout: new LayoutStore(),
    map: new MapStore(),
    geography: new GeographyStore(),
    filters: new FiltersStore(),
    headings: new HeadingsStore(),
    playList: new PlayListStore(),
  }
})
export default class ApplicationStore extends TypedStore {

  private layout: LayoutStore;

  private map: MapStore;

  private playList: PlayListStore;

  private geography: GeographyStore;

  private filters: FiltersStore;

  private headings: HeadingsStore;

}

