import './videoTime.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class VideoTimeComponent extends Vue {
  @Prop({ default: 0 })
  time: number;

  get timeVideo() {
    // const time = this.time / 60
    return this.time;
  }
}


