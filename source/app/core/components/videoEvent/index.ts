
import $ from 'jquery';
import './videoEvent.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class VideoEventComponent extends Vue {
  @Prop({ default: 'right' })
  position: string;

  @Prop({ default: '0' })
  text: string;

  private isShare: any = null;

  mounted() {
    this.isShare = document.getElementsByClassName('videoEvent__block_share');
    console.log(this.isShare[0]);
    this.isShare[0].addEventListener('click', this.clickShare, false);
  }

  private clickShare(event) {
    console.log(event.target);

    $(event.target).addClass('active');
  }
}
