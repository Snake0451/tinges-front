import './tag.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class TagComponent extends Vue {

  @Prop({ default: 'Тег' })
  text: string;

  @Prop({ default: 'small' })
  size: string;

  @Prop({ default: true })
  isRounded: boolean;

  get sizeClass(): string {
    return `${this.size}_tag`;
  }

}
