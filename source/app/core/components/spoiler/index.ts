import './spoiler.scss';

import Vue from 'vue';
import $ from 'jquery';
import Component from 'vue-class-component';
import { Prop, Watch } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class SpoilerComponent extends Vue {

  @Prop({ default: 8 })
  numToHide: number;

  @Prop({ default: 'spoiler' })
  classes: string;

  @Watch('isOpened')
  onSpolierToggle(value) {
    if (value) {
      this.showItems();
    }
    else {
      this.hideItems();
    }
  }

  private isSpoiler: boolean = false;

  private isOpened: boolean = false;

  private childs: any = null;

  mounted() {
    this.init();
  }

  updated() {
    this.init();
  }

  private init() {
    this.childs = $(this.$refs['spoiler']).find('.spoiler__items').children();

    if (this.childs.length >= this.numToHide) {
      this.isSpoiler = true;

      this.hideItems();
    }
    else {
      this.isSpoiler = false;
    }
  }

  private toggleSpoilerContent(): void {
    this.isOpened = !this.isOpened;
  }

  private showItems(): void {
    this.childs.each((index, element) => {
      $(element).show();
    });
  }

  private hideItems() {
    this.childs.each((index, element) => {
      if (index >= this.numToHide)  {
        $(element).hide();
      }
    });
  }
}
