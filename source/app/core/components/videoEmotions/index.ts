import './videoEmotions.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

@Component({
  template: require('./template.pug'),
})
export default class TagComponent extends Vue {
  @Prop({ default: [] })
  emotions: Array<string>;
}


