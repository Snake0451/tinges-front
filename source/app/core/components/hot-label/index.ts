import './hot-label.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';

@Component({
  template: require('./template.pug')
})
export default class HotLabelComponent extends Vue {

  @Prop({ required: true })
  text: string;

  @Prop({ default: 0 })
  positionX: number;

  @Prop({ default: 0 })
  positionY: number;

  get position(): Object {
    return { left: `${this.positionX}px`, top: `${this.positionY}px` };
  }

}
