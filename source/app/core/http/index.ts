import axios from 'axios';

export const HTTP = axios.create({
  baseURL: (process.env.NODE_ENV !== 'production') ? 'http://vmap.atwinta.ru/api/v1/' : 'api/v1/'
});
