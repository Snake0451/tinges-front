export interface ICity {
  country_geoid: number;

  location_geoid: number;

  lat: number;

  lng: number;

  name_ru: string;

  population: number;
}

export interface ICountry {
  capital_geoid: number;

  country_geoid: number;

  lat: number;

  lng: number;

  name_ru: string;

  locations: Array<ICity>;
}
