import './cities.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { State, Action } from 'vuex-class';

import { ICity } from '../../interfaces';
import { ITag } from '../../../filters/interfaces';

@Component({
  template: require('./template.pug'),
})
export default class CitiesComponent extends Vue {

  @Prop({ default: 0 })
  selectedCountry: number;

  @Prop({ default: () => [] })
  cities: Array<ICity>;

  @Action('A_ADD_TAG') addTag;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggle;

  private addCityToTagsList(geoId: number, name: string, countryGeoId: number) {
    let newTag = <ITag<{name: string, type: string, country_geoid: number}>>{
      [geoId]: {
        name: name,
        type: 'city',
        country_geoid: countryGeoId
      }
    };

    this.addTag(newTag);
    this.toggle(false);
  }

}
