import './countries.scss';

import Vue from 'vue';
import $ from 'jquery';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';
import { Action } from 'vuex-class';

import { ICountry } from '../../interfaces';
import { ITag } from '../../../filters/interfaces';

@Component({
  template: require('./template.pug'),
})
export default class CountriesComponent extends Vue {

  @Prop({ default: () => [] })
  countries: Array<ICountry>;

  @Action('A_ADD_TAG') addTag;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggle;

  @Action('A_CHANGE_COUNTRY') changeCountry;

  get countriesByFirstLetter() {
    let mapedCountries = {};

    [].forEach.call(this.countries, function(element) {

      let firstLetter = element.name_ru.split('')[0];

      if (mapedCountries.hasOwnProperty(firstLetter)) {
          mapedCountries[firstLetter].push(element);
      }
      else {
          mapedCountries[firstLetter] = [element];
      }
    });

    return mapedCountries;

  }

  private changeCountryAndSetTag(geoId: number, name: string) {
    this.changeCountry(geoId);

    let newTag = <ITag<{name: string, type: string, country_geoid: number}>>{
      [geoId]: {
        name: name,
        type: 'country',
        country_geoid: geoId,
      }
    };

    this.addTag(newTag);
    this.toggle(false);
  }

}
