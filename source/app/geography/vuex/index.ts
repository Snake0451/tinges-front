import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import * as TYPES from './types';
import { HTTP } from '../../core/http';

import { ICountry } from '../interfaces';

@module
export default class GeographyStore extends TypedStore {

  public countries: Array<ICountry> = [];

  public selectedCountry: number = 0;

  @action
  [TYPES.A_FETCH_COUNTRIES]() {
    HTTP.get(`countries`)
      .then(response => {
        this.commit(TYPES.M_SET_COUNTRIES, response.data);
      });
  }

  @action
  [TYPES.A_CHANGE_COUNTRY](geoId: number) {
    this.commit(TYPES.M_SET_COUNTRY, geoId);
  }

  @mutation
  [TYPES.M_SET_COUNTRIES](data: Array<ICountry>) {
    this.countries = data;
  }

  @mutation
  [TYPES.M_SET_COUNTRY](geoId: number) {
    this.selectedCountry = geoId;
  }
}

