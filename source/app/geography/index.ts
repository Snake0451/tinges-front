import './geography.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Watch } from 'vue-property-decorator';

import { ICity, ICountry } from './interfaces';

import CountriesComponent from './components/countries/';
import CitiesComponent from './components/cities/';

Component.registerHooks([
  'beforeRouteEnter'
]);

@Component({
  template: require('./template.pug'),
  components: {
    'v-countries': CountriesComponent,
    'v-cities': CitiesComponent
  }
})
export default class GeographyComponent extends Vue {

  @State(state => state.geography.countries) countries: Array<ICountry>;

  @State(state => state.geography.selectedCountry) selectedCountry: number;

  @State(state => state.filters.searchQuery) searchQuery: string;

  @Action('A_FETCH_COUNTRIES') fetchCountries;

  private selectedCountryName: string = '';

  private filteredCountries: Array<ICountry> = [];

  @Watch('countries', { deep: true })
  onCountriesChanged(value) {
    this.filteredCountries = value;
  }

  @Watch('searchQuery')
  onSearchQueryChanged(value) {
    if (value.length >= 3) {
      this.filteredCountries = this.countries.filter(function(element) {
        let name = element.name_ru.toLowerCase();
        if (name.indexOf(value.toLowerCase()) !== -1) {
          return element;
        }
      });
    }
    else if (value.length === 0) {
      this.filteredCountries = this.countries;
    }
  }

  @Watch('selectedCountryName')
  onSelectedCountryChanged(value) {
    const container = document.querySelector('#js-geography');
    container.scrollTop = 0;
  }

  created() {
    this.fetchCountries();
  }

  get citiesByCountry(): Array<ICity>  {
    let cities: Array<ICity> = [];

    <any>this.countries.filter((country: ICountry) => {
      if (country.country_geoid === this.selectedCountry) {
        cities = country.locations;
        this.selectedCountryName = country.name_ru;
      }
    });

    return cities;
  }

}
