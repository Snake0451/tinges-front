import './layout.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State } from 'vuex-class';
import select from 'vue-select';


import MapComponent from '../gmap';
import SidebarComponent from './components/sidebar';
import ContentComponent from './components/content';
import FormErrorComponent from './components/form-error';
import PlayListComponent from '../playList';
import NavigationComponent from '../navigation/';
import ThemeToggleComponent from './components/theme-toggle/';
import SidebarMobileButtonComponent from './components/sidebar-mobile-btn/';
import SearchComponent from '../filters/components/search/';
import TagFiltersListComponent from '../filters/components/tag-filters-list';

@Component({
  template: require('./template.pug'),
  components: {
    'v-map': MapComponent,
    'v-sidebar': SidebarComponent,
    'v-content': ContentComponent,
    'v-playList': PlayListComponent,
    'v-navigation': NavigationComponent,
    'v-theme-toggle': ThemeToggleComponent,
    'v-sidebar-mobile-btn': SidebarMobileButtonComponent,
    'v-search': SearchComponent,
    'v-form-error': FormErrorComponent,
    'v-tag-filters-list': TagFiltersListComponent,
    'v-select': select
  }
})
export default class LayoutComponent extends Vue {

  @State(state => state.layout.theme) theme: string;

  @State(state => state.route.name) routeName: string;

  @State(state => state.layout.isSidebarOpened) isSidebarOpened: boolean;

  @State(state => state.layout.isContentWindowOpened) isContentWindowOpened: boolean;

  @State(state => state.layout.isModalErrorOpened) isModalErrorOpened: boolean;

  sort: object = [
    { sortCode: "popularity", sortName: "popularity" },
    { sortCode: "durability", sortName: "durability" }
  ];

  selectedSort: object = { sortCode: 'popularity', sortName: 'popularity' };
}
