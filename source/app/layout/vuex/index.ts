import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import { HTTP } from '../../core/http';
import * as TYPES from './types';

import { IErrorFields } from '../interfaces';

@module
export default class LayoutStore extends TypedStore {

  public modernizr = window['Modernizr'];

  public isSidebarOpened = this.modernizr.mq('(min-width: 1366px)');

  public isContentWindowOpened = false;

  public isModalErrorOpened = false;

  public isSidebarAtMaxScroll = false;

  public sendError = false;

  public theme = 'day-theme';

  @action
  [TYPES.A_TOGGLE_SIDEBAR](data?: boolean) {
    this.commit(TYPES.M_TOGGLE_SIDEBAR, data);
  }

  @action
  [TYPES.A_TOGGLE_CONTENT_WINDOW](data?: boolean) {
    this.commit(TYPES.M_TOGGLE_CONTENT_WINDOW, data);
  }

  @action
  [TYPES.A_TOGGLE_MODAL_ERROR](data?: boolean) {
    this.commit(TYPES.M_TOGGLE_MODAL_ERROR, data);
  }

  @action
  [TYPES.A_TOGGLE_THEME]() {
    this.commit(TYPES.M_TOGGLE_THEME);
  }

  @action
  [TYPES.A_TOGGLE_SIDEBAR_MAX_SCROLL]() {
    this.commit(TYPES.M_TOGGLE_SIDEBAR_MAX_SCROLL);
  }

  @action
    [TYPES.A_SEND_ERROR](data: IErrorFields) {
    HTTP.post(`send/error`, { message: data.message, url: data.url })
      .then(response => {
        this.commit(TYPES.M_SEND_ERROR, true);
      })
      .catch(error => {
        this.commit(TYPES.M_SEND_ERROR, false);
      });
  }

  @mutation
  [TYPES.M_TOGGLE_SIDEBAR](data?: boolean) {
    if (data !== undefined) {
      this.isSidebarOpened = data;
    }
    else {
      this.isSidebarOpened = !this.isSidebarOpened;
    }
  }

  @mutation
  [TYPES.M_TOGGLE_CONTENT_WINDOW](data?: boolean) {
    if (data !== undefined) {
      this.isContentWindowOpened = data;
    }
    else {
      this.isContentWindowOpened = !this.isContentWindowOpened;
    }
  }

  @mutation
  [TYPES.M_TOGGLE_MODAL_ERROR](data?: boolean) {
    if (data !== undefined) {
      this.isModalErrorOpened = data;
    }
    else {
      this.isModalErrorOpened = !this.isModalErrorOpened;
    }
  }

  @mutation
  [TYPES.M_TOGGLE_THEME]() {
    this.theme = (this.theme === 'day-theme') ? 'night-theme' : 'day-theme';
  }

  @mutation
  [TYPES.M_TOGGLE_SIDEBAR_MAX_SCROLL]() {
    this.isSidebarAtMaxScroll = !this.isSidebarAtMaxScroll;
  }

  @mutation
    [TYPES.M_SEND_ERROR](data: boolean) {
    this.sendError = data;
  }
}

