export const A_TOGGLE_SIDEBAR = 'A_TOGGLE_SIDEBAR';
export const A_TOGGLE_CONTENT_WINDOW = 'A_TOGGLE_CONTENT_WINDOW';
export const A_TOGGLE_MODAL_ERROR = 'A_TOGGLE_MODAL_ERROR';
export const A_TOGGLE_THEME = 'A_TOGGLE_THEME';
export const A_TOGGLE_SIDEBAR_MAX_SCROLL = 'A_TOGGLE_SIDEBAR_MAX_SCROLL';
export const A_SEND_ERROR = 'A_SEND_ERROR';

export const M_TOGGLE_SIDEBAR = 'M_TOGGLE_SIDEBAR';
export const M_TOGGLE_THEME = 'M_TOGGLE_THEME';
export const M_TOGGLE_SIDEBAR_MAX_SCROLL = 'M_TOGGLE_SIDEBAR_MAX_SCROLL';
export const M_TOGGLE_CONTENT_WINDOW = 'M_TOGGLE_CONTENT_WINDOW';
export const M_TOGGLE_MODAL_ERROR = 'M_TOGGLE_MODAL_ERROR';
export const M_SEND_ERROR = 'M_SEND_ERROR';

