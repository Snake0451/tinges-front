export interface IErrorFields {
  message: string;

  url: string;
}
