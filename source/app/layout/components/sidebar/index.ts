import './sidebar.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';

@Component({
  template: require('./template.pug')
})
export default class SidebarComponent extends Vue {

  @State(state => state.route) route;

  @State(state => state.layout.modernizr) modernizr;

  @State(state => state.layout.isSidebarAtMaxScroll) isSidebarAtMaxScroll: boolean;

  @Action('A_TOGGLE_SIDEBAR') toggle;

  @Action('A_TOGGLE_SIDEBAR_MAX_SCROLL') toggleSidebarMaxScroll;

  created() {
    window.addEventListener('resize', (event) => {
      this.toggle(this.modernizr.mq('(min-width: 1025px)'));
    });
  }

  getTitle() {
    switch (this.route.name) {
      case 'playList':
        return 'Последние статьи';

      case 'geography':
        return 'География';

      case 'headings':
        return 'Рубрики';
    }
  }

}
