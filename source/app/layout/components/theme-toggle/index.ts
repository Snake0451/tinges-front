import './theme-toggle.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Action } from 'vuex-class';

@Component({
  template: require('./template.pug'),
})
export default class ThemeToggleComponent extends Vue {

  @Action('A_TOGGLE_THEME') toggleTheme;

}
