import './form-error.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Action, State } from 'vuex-class';

import { IArticle } from '../../../playList/intefaces';

import ModalComponent from '../modal';

@Component({
  template: require('./template.pug'),
  components: {
    'v-modal': ModalComponent
  }
})
export default class FormErrorComponent extends Vue {

  private message: string = '';

  private loading: boolean = false;

  private success: boolean = false;

  @Action('A_TOGGLE_MODAL_ERROR') toggleModalError;

  @Action('A_SEND_ERROR') sendError;

  @State(state => state.playList.article) article: IArticle;

  @State(state => state.layout.isModalErrorOpened) isModalErrorOpened: boolean;

  private submit() {
    this.loading = true;
    this.sendError({
      message: this.message,
      url: this.article.url
    })
      .then(() => {
        this.success = true;
        setTimeout(this.clear, 1500);
      });
  }

  private clear() {
    this.loading = false;
    this.success = false;
    this.message = '';
    this.toggleModalError();
  }

}
