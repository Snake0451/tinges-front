import './content.scss';

import Vue from 'vue';
import * as moment from 'moment';
moment.locale('Ru-ru');
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Watch } from 'vue-property-decorator';

import TagComponent from '../../../core/components/tag';
import HotLabelComponent from '../../../core/components/hot-label';
import SharingComponent from '../../../sharing';
import { IMapLocation } from '../../../gmap/interfaces';

import { IArticle } from '../../../playList/intefaces';
import { ITag } from '../../../filters/interfaces';

@Component({
  template: require('./template.pug'),
  components: {
    'v-tag': TagComponent,
    'v-hot-label': HotLabelComponent,
    'v-sharing': SharingComponent
  }
})
export default class ContentComponent extends Vue {

  @State(state => state.map.locations) locations: Array<IMapLocation>;

  @State(state => state.playList.article) article: IArticle;

  @State(state => state.layout.isModalErrorOpened) isModalErrorOpened: boolean;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggleContent;

  @Action('A_TOGGLE_MODAL_ERROR') toggleModalError;

  @Action('A_ADD_TAG') addTag;

  @Watch('article')
  onArticleChanged() {
    document.getElementById('js-content-window').scrollTop = 0;
  }

  mounted() {
    document.addEventListener('keydown', this.handleEscPress);
  }

  private handleEscPress(e): void {
    const event = e || window.event;

    if (event.keyCode === 27) {
      if (this.isModalErrorOpened) {
        this.toggleModalError(false);
      } else {
        this.toggleContent(false);
      }
    }
  }

  private filterByLocation(cityName) {
    let newTag = <ITag<{name: string, type: string}>>{
      [this.article.location_geoid]: {
        name: cityName,
        type: 'city'
      }
    };

    this.addTag(newTag);
  }

  get articleCity() {
    const city = this.locations.filter(loc => loc.location_geoid === this.article.location_geoid)[0];
    return city ? city.name_ru : 'Город';
  }

  get formatedDate() {
    return moment(this.article.created_at).format('DD MMMM YYYY, H:mm').toString();
  }

  get isNew(): boolean {
    let itemDate = moment(this.article.created_at).format('YYYY-MM-DD');
    let dateLimit = moment().subtract(7, 'd').format('YYYY-MM-DD');

    if (itemDate > dateLimit) {
      return true;
    }
    return false;
  }
}
