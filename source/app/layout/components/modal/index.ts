import './modal.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { Prop } from 'vue-property-decorator';


@Component({
  template: require('./template.pug')
})
export default class ModalComponent extends Vue {

  @Prop({ default: false })
  open: boolean;
}
