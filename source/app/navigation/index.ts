import './navigation.scss';

import Vue from 'vue';
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';

Component.registerHooks([
  'beforeRouteEnter'
]);

@Component({
  template: require('./template.pug')
})
export default class NavigationComponent extends Vue {

  private routes: Array<Object>;

  @State(state => state.layout.isSidebarOpened) isSidebarOpened: boolean;

  @Action('A_TOGGLE_SIDEBAR') toggleSidebar;

  created() {
    this.routes = this.$router['options'].routes;
  }

  private openSidebarIfNeed(): void {
    if (!this.isSidebarOpened) {
      this.toggleSidebar(true);
    }
  }

}
