/// <reference path="../core/typings/json.d.ts" />

import '../gmap/gmap.scss';
import './widget.scss';

import bubblePin from 'assets/images/icons/bubble-pin.svg';
import * as dayTheme from '../gmap/themes/day.theme.json';

import Vue from 'vue';
import $ from 'jquery';
import Component from 'vue-class-component';

import GoogleMapsLoader from 'google-maps';

import { IMapCoords } from '../gmap/interfaces';

const getParameter = require('get-parameter');

@Component({
  template: require('./template.pug'),
})
export default class WidgetComponent extends Vue {

  private map: any = null;

  private marker: any = null;

  private params: any = getParameter();

  mounted() {
    GoogleMapsLoader.KEY = 'AIzaSyBd-BsIisYvN3i60aMPNWRQmpxq2ZASKWk';
    GoogleMapsLoader.LANGUAGE = 'ru';
    GoogleMapsLoader.load((google) => {
      this.initialize(google);
    });
  }

  private initialize(google: any): void {
    this.map = new google.maps.Map(document.getElementById('map__canvas'), {
      center: <IMapCoords>{ lat: parseFloat(this.params.lat), lng: parseFloat(this.params.lng) },
      disableDefaultUI: true,
      minZoom: 3,
      maxZoom: 14,
      styles: dayTheme,
      zoom: 9
    });

    this.marker = new google.maps.Marker({
      position: <IMapCoords>{ lat: parseFloat(this.params.lat), lng: parseFloat(this.params.lng) },
      map: this.map,
      icon: bubblePin
    });
  }
}
