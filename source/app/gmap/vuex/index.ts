import Vue from 'vue';
import TypedStore from '../../core/store/typedstore';
import { action, module, mutation } from 'vuex-ts-decorators';
import * as TYPES from './types';
import { HTTP } from '../../core/http';

import { IMapBounds, IMapLocation } from '../interfaces';

@module
export default class MapStore extends TypedStore {

  private locations: Array<IMapLocation> = [];

  private bounds: IMapBounds = <IMapBounds>{};

  private currentLocation: IMapLocation = <IMapLocation>{};

  get mapBounds(): IMapBounds {
    return this.bounds;
  }

  @action
  [TYPES.A_FETCH_LOCATIONS]() {
    HTTP.get(`locations`)
      .then(response => {
        this.commit(TYPES.M_SET_LOCATIONS, response.data);
      });
  }

  @action
  [TYPES.A_UPDATE_BOUNDS](data: IMapBounds) {
    this.commit(TYPES.M_SET_BOUNDS, data);
  }

  @action
  [TYPES.A_SELECT_LOCATION](location: IMapLocation) {
    this.commit(TYPES.M_SET_CURRENT_LOCATION, location);
  }

  @mutation
  [TYPES.M_SET_LOCATIONS](data: Array<IMapLocation>) {
    this.locations = data;
  }

  @mutation
  [TYPES.M_SET_BOUNDS](newBounds: IMapBounds) {
      this.bounds = newBounds;
  }

  @mutation
  [TYPES.M_SET_CURRENT_LOCATION](location: IMapLocation) {
    this.currentLocation = location;
  }
}

