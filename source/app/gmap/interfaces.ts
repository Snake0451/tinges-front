import { IArticle } from '../playList/intefaces';

export interface IMapBounds {
  north: number;

  south: number;

  east: number;

  west: number;
}

export interface IMapLocation {
  articles_count: number;

  country_geoid: number;

  last_article: IArticle;

  last_article_id: number;

  lng: number;

  lat: number;

  location_geoid: number;

  name_ru: string;

  population: number;

  categories?: any;
}

export interface IMapCoords {
  lat: number;

  lng: number;
}

export interface IGMapOptions {
  backgroundColor?: string;

  center: IMapCoords;

  clickableIcons?: boolean;

  disableDefaultUI?: boolean;

  zoomControl?: boolean;

  zoomControlOptions?: any;

  disableDoubleClickZoom?: boolean;

  mapTypeId?: any;

  maxZoom?: number;

  minZoom?: number;

  zoom?: number;

  styles?: any;
}

export interface IGClustererOptions {
  styles?: any;
}
