/// <reference path="../core/typings/json.d.ts" />

import './gmap.scss';

import * as moment from 'moment';
import { debounce } from 'lodash';
// import { Bar } from 'vue-chartjs';
moment.locale('Ru-ru');

import * as dayTheme from './themes/day.theme.json';
import * as nightTheme from './themes/night.theme.json';

import Vue from 'vue';
import $ from 'jquery';



import GModalComponent from './components/gmodal';
import GZoomComponent from './components/gzoom';
import GMarker from './components/gmarker';

import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Watch, Prop } from 'vue-property-decorator';

import GoogleMapsLoader from 'google-maps';

import MarkerClusterer from 'node-js-marker-clusterer';

import {
  IMapBounds,
  IMapLocation,
  IGMapOptions,
  IGClustererOptions,
  IMapCoords
  } from './interfaces';
import { ITag } from '../filters/interfaces';
import { IArticle } from '../playList/intefaces';
import { IPagination } from '../core/interfaces';

const cluster = require('assets/images/icons/cluster.png');
// const bubblePin = require('assets/images/icons/bubble-pin.png');

@Component({
  template: require('./template.pug'),
  components: {
    'v-gmarker': GMarker,
    'v-gmodal': GModalComponent,
    'v-zoom': GZoomComponent
  }
})
export default class GMapComponent extends Vue {

  /** All available locations on map */
  @State(state => state.map.locations) locations: Array<IMapLocation>;

  /** Current layout theme */
  @State(state => state.layout.theme) theme: string;

  /** List of tags selected by the user */
  @State(state => state.filters.preparedTags) preparedTags: ITag<string>;

  /** Location selected by the user */
  @State(state => state.map.currentLocation) currentLocation: IMapLocation;

  /** list of articles for selected location */
  @State(state => state.playList.articlesForlocation) articlesForlocation: IPagination;

  /** Load locations list from server */
  @Action('A_FETCH_LOCATIONS') fetchLocations;

  /** Update current map bounds in store */
  @Action('A_UPDATE_BOUNDS') updateBounds;

  /** Sets locations selected by the user */
  @Action('A_SELECT_LOCATION') selectLocation;

  /** Loads articles for selected location */
  @Action('A_FETCH_ARTICLES_FOR_LOCATION') getArticlesForLocation;

  @Prop({ default: () => {
    return <IGMapOptions> {
      center: <IMapCoords>{ lat: 55.7494733, lng: 37.3523257 },
      disableDefaultUI: true,
      minZoom: 3,
      maxZoom: 12,
      styles: dayTheme,
      zoom: 4
    };
  }})
  private mapOptions: IGMapOptions;

  private datacollection: object = {
    labels: [1, 2],
    datasets: [
      {
        label: 'Data One',
        backgroundColor: '#f87979',
        data: [2, 3]
      }, {
        label: 'Data One',
        backgroundColor: '#f87979',
        data: [3, 4]
      }
    ]
  }


  @Prop({ default: () => {
    return <IGClustererOptions> {
      styles: [{
        width: 30,
        height: 51,
        url: cluster,
        fontFamily: 'ProximaNova',
        textSize: 13,
        textColor: '#333333',
        backgroundPosition: '0 0'
      }]
    };
  }})
  private clustererOptions: IGClustererOptions;

  private bounds: IMapBounds = <IMapBounds>{};

  private map: any = null;

  private google: any = null;

  private clusterer: MarkerClusterer = null;

  private markers: Array<any> = [];

  private infoWindow: any = null;

  private isInfoOpened: boolean = false;

  /** @type {Array<IMapLocation>}
  * list of the locations by applied tags. Vue always renders only this list.
  */
  private filteredLocations: Array<IMapLocation> = [];

  private currentLocationArticle: IArticle = <IArticle>{};

  private currentLocationArticleIndex: number = 0;

  /** Change map style depends on theme */
  @Watch('theme')
  onBoundsChanged(value) {
    if (value === 'day-theme') {
      this.mapOptions.styles = dayTheme;
    }
    else {
      this.mapOptions.styles = nightTheme;
    }

    this.map.setOptions(this.mapOptions);
  }

  /** Assign locations form store to local locations list */
  @Watch('locations')
  onCountriesChanged(value) {
    this.filteredLocations = value;

    this.updateMarkers();

    this.clusterer = new MarkerClusterer(this.map, this.markers, this.clustererOptions);
  }

  @Watch('preparedTags')
  onPreparedTagsChanged(value) {
    if (Object.keys(value).length !== 0 && value.constructor === Object) {
      let tempFilter = this.locations.filter((location, index) => {
        if (
          value.hasOwnProperty(location.country_geoid) ||
          value.hasOwnProperty(location.location_geoid) ||
          (location.categories && value.hasOwnProperty(location.categories[0]))
        ) {
          return location;
        }
      });

      this.filteredLocations = tempFilter;
    }
    else {
      this.filteredLocations = this.locations;
    }

    this.clearMarkers();
    this.updateMarkers();

    let bounds = new this.google.maps.LatLngBounds();

    for (let i in this.markers) {
      bounds.extend(this.markers[i].getPosition());
    }
    this.map.fitBounds(bounds);

    this.clusterer = new MarkerClusterer(this.map, this.markers, this.clustererOptions);
    // this.clusterer.addMarkers(this.markers);
  }

  created() {
    if (this.google) {
      this.fetchLocations();
    }
  }

  mounted() {
    GoogleMapsLoader.KEY = 'AIzaSyBd-BsIisYvN3i60aMPNWRQmpxq2ZASKWk';
    GoogleMapsLoader.LANGUAGE = 'ru';
    GoogleMapsLoader.load((googleObj) => {
      this.google = googleObj;
      this.fetchLocations();
      this.initialize();
    });
  }

  private initialize(): void {
    if (window.innerWidth < 1366) {
      Object.assign(this.mapOptions, { minZoom: 4, maxZoom: 12 });
    }



    Object.assign(this.mapOptions, { mapTypeId: this.google.maps.MapTypeId.ROADMAP });
    this.map = new this.google.maps.Map(document.getElementById('map__canvas'), this.mapOptions);

    this.map.addListener('bounds_changed', debounce(this.saveBoundsLocaly, 500));
    this.map.addListener('idle', debounce(this.saveBoundsToStore, 500));
    this.map.addListener('dragend', debounce(this.checkBounds, 500));
    this.map.addListener('zoom_changed', this.closeInfoWindow);

    this.infoWindow = new this.google.maps.InfoWindow({
      content: this.$refs['modal']
    });

    this.infoWindow.addListener('domready', () => {
      const element = $('.gm-style-iw').prev().hide();
    });



    var southWest = new google.maps.LatLng(40.744656, -74.005966);
    var northEast = new google.maps.LatLng(34.052234, -118.243685);
    var lngSpan = northEast.lng() - southWest.lng();
    var latSpan = northEast.lat() - southWest.lat();

    var markers = [];
    for (var i = 1; i < 100; i++) {

      var location = new this.google.maps.LatLng(southWest.lat() + latSpan * Math.random(), southWest.lng() + lngSpan * Math.random());

      var marker = new this.google.maps.Marker({
          position: location,
          map: this.map
      });

      markers.push(marker);
    }


    function HTMLMarker(lat,lng){
      this.lat = lat;
      this.lng = lng;
      this.pos = new google.maps.LatLng(lat,lng);
    }

    HTMLMarker.prototype = new google.maps.OverlayView();
    HTMLMarker.prototype.onRemove = function(){}

    //init your html element here
    HTMLMarker.prototype.onAdd = function(){
      let div = document.createElement('DIV');
      div.className = "arrow_box";
      div.innerHTML = '<v-gmarker></v-gmarker>';
      var panes = this.getPanes();
      panes.overlayImage.appendChild(div);
    }

    HTMLMarker.prototype.draw = function(){
        var overlayProjection = this.getProjection();
        var position = overlayProjection.fromLatLngToDivPixel(this.pos);
        var panes = this.getPanes();
        panes.overlayImage.style.left = position.x + 'px';
        panes.overlayImage.style.top = position.y - 30 + 'px';
    }

    //to use it
    var htmlMarker = new HTMLMarker(44.73532729516236, 14.806330871582077);
    htmlMarker.setMap(this.map);



    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        let pos = <IMapCoords>{
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        this.map.setCenter(pos);
      });
    }
  }

  private offsetCenter(latLng, offsetX, offsetY) {
    const scale = Math.pow(2, this.map.getZoom());

    const worldCoordinateCenter = this.map.getProjection().fromLatLngToPoint(latLng);
    const pixelOffset = new google.maps.Point((offsetX / scale) || 0, (offsetY / scale) || 0);

    const worldCoordinateNewCenter = new google.maps.Point(
      worldCoordinateCenter.x - pixelOffset.x,
      worldCoordinateCenter.y + pixelOffset.y
    );

    return this.map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);
  }

  private updateMarkers(): void {
    this.markers = this.filteredLocations.map((element, index) => {
      let mark = new this.google.maps.Marker({
        position: { lat: element.lat, lng: element.lng },
        icon: cluster
      });

      mark.addListener('click', (event) => {
        this.currentLocationArticle = element.last_article;

        this.selectLocation(element).then(response => {
          if (element.articles_count > 1) {
            this.getArticlesForLocation({
              locationId: this.currentLocation.location_geoid
            });
          }
        });

        const isMobile = window.innerWidth < 1366;
        const shiftModal = isMobile ? -260 : -130;
        const panCoords = this.offsetCenter(event.latLng, 0, shiftModal);

        // fix for "jumping" gmodals after click
        setTimeout(() => {
          this.map.panTo(panCoords);
          // this.mapOptions.center = panCoords;
        }, 300);

        this.isInfoOpened = true;
        this.currentLocationArticleIndex = 0;
        this.infoWindow.open(this.map, mark);

        let iwContainer = $('.gm-style-iw').parent();
        iwContainer.stop().hide();
        iwContainer.fadeIn(1000);
      });

      return mark;
    });
  }

  private clearMarkers(): void {
    if (this.markers.length > 0) {
      for (let i = 0; i < this.markers.length; i++) {
        this.markers[i].setMap(null);
      }
    }

    if (typeof this.clusterer !== 'undefined') {
      this.clusterer.clearMarkers();
    }
  }

  private saveBoundsLocaly(): void {
    const newBounds = this.map.getBounds();
    if (typeof newBounds !== 'undefined') {
      this.bounds = {
        north: newBounds.getNorthEast().lat().toFixed(6),
        south: newBounds.getSouthWest().lat().toFixed(6),
        east: newBounds.getNorthEast().lng().toFixed(6),
        west: newBounds.getSouthWest().lng().toFixed(6),
      };
    }
  }

  private saveBoundsToStore(): void {
    this.updateBounds(this.bounds);
    document.getElementById('js-playList').scrollTop = 0;
  }

  private checkBounds() {
    const bounds = this.map.getBounds();
    const center = this.map.getCenter();

    if (bounds === undefined) return;

    const latNorth = bounds.getNorthEast().lat();
    const latSouth = bounds.getSouthWest().lat();
    let newLat;

    if (latNorth < 85 && latSouth > -85) return;
    else {
      if (latNorth > 85 && latSouth < -85) return;
      else {
        if (latNorth > 85)
          newLat =  center.lat() - (latNorth - 85);
        if (latSouth < -85)
          newLat =  center.lat() - (latSouth + 85);
      }
    }

    if (newLat) {
      const newCenter = new this.google.maps.LatLng(newLat, center.lng());
      this.map.setCenter(newCenter);
    }
  }

  private closeInfoWindow() {
    this.infoWindow.close();
    this.isInfoOpened = false;
  }

  private zoomPros() {
    this.map.setZoom(this.map.zoom + 1);
  }

  private zoomMinus() {
    this.map.setZoom(this.map.zoom - 1);
  }

  /** Switches to next article for this location */
  private nextArticleForLocation() {
    let next;

    if (this.currentLocationArticleIndex < this.articlesForlocation['data'].length - 1) {
      this.currentLocationArticleIndex++;

      next = this.articlesForlocation['data'][this.currentLocationArticleIndex];
      if (next !== undefined) {
        this.currentLocationArticle = next;
      }
    }
    else {
      if (this.articlesForlocation.current_page !== this.articlesForlocation.last_page) {
        this.getArticlesForLocation({
            locationId: this.currentLocation.location_geoid,
            page: this.articlesForlocation.current_page + 1
        }).then(response => {
          this.currentLocationArticleIndex = 0;
          next = response['data'][this.currentLocationArticleIndex];

          if (next !== undefined) {
            this.currentLocationArticle = next;
          }
        });
      }
    }
  }

  /** Switches to prev article for this location */
  private prevArticleForLocation() {
    let prev;

    if (this.currentLocationArticleIndex > 0) {
      this.currentLocationArticleIndex--;

      prev = this.articlesForlocation['data'][this.currentLocationArticleIndex];
      if (prev !== undefined) {
        this.currentLocationArticle = prev;
      }
    }
    else {
      if (this.articlesForlocation.current_page !== 1) {
        this.getArticlesForLocation({
            locationId: this.currentLocation.location_geoid,
            page: this.articlesForlocation.current_page - 1
        }).then(response => {
          this.currentLocationArticleIndex = 4;
          prev = response['data'][this.currentLocationArticleIndex];

          if (prev !== undefined) {
            this.currentLocationArticle = prev;
          }
        });
      }
    }
  }

  get currentLocationArticlesNum(): number {
    return this.currentLocation.articles_count || 0;
  }

  get isNext(): boolean {
    if (this.articlesForlocation.next_page_url === null
        && this.currentLocationArticleIndex === this.articlesForlocation.data.length - 1) {
      return true;
    }

    return false;
  }

  get isPrev(): boolean {
    if (this.articlesForlocation.prev_page_url === null && this.currentLocationArticleIndex === 0) {
      return true;
    }

    return false;
  }

}
