import './gmarker.scss';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
  template: require('./template.pug'),
  components: {
    'v-bar': (resolve) => (require as any)(['vue-chartjs'], module => resolve(module.default))
  }
})
export default class GZoomComponent extends Vue {
  // private onPlus() {
  //   this.$emit('on-plus');
  // }

  // private onMinus() {
  //   this.$emit('on-minus');
  // }
}
