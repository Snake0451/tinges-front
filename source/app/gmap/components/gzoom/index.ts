import './gzoom.scss';

import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
  template: require('./template.pug')
})
export default class GZoomComponent extends Vue {
  private onPlus() {
    this.$emit('on-plus');
  }

  private onMinus() {
    this.$emit('on-minus');
  }
}
