import './gmodal.scss';

import Vue from 'vue';
import * as moment from 'moment';
moment.locale('Ru-ru');
import Component from 'vue-class-component';
import { State, Action } from 'vuex-class';
import { Prop } from 'vue-property-decorator';

import { IArticle } from '../../../playList/intefaces';
import TagComponent from '../../../core/components/tag';
import SpoilerComponent from '../../../core/components/spoiler';

import '../../../core/directives/date-format';

@Component({
  template: require('./template.pug'),
  components: {
    'v-tag': TagComponent,
    'v-spoiler': SpoilerComponent
  }
})
export default class GModalComponent extends Vue {

  @Prop({ default: () => <IArticle>{
    id: 0,
    image: '/assets/images/map_img.jpg',
    location_geoid: 0,
    short_content: '',
    tags: [],
    title: '',
    previews: {},
    url: '',
    created_at: ''
  }})
  data: IArticle;

  @Prop({default: 'Город'})
  geoname: string;

  @Prop({default: false})
  nextDisabled: boolean;

  @Prop({default: true})
  prevDisabled: boolean;

  @Prop({ default: 1 })
  articlesNum: number;

  @Prop({ default: 1 })
  articleIndex: number;

  @Prop({ default: 1 })
  page: number;

  @Prop({ default: false })
  isActive: boolean;

  @State(state => state.layout.isContentWindowOpened) isContentWindowOpened: boolean;

  @Action('A_FETCH_ARTICLE') openArticle;

  @Action('A_TOGGLE_CONTENT_WINDOW') toggleContentWindow;

  private getArticle(id: number) {
    this.openArticle(id).then(response => {
      if (!this.isContentWindowOpened) {
        this.toggleContentWindow();
      }
    });
  }

  private onClose() {
    this.$emit('on-close');
  }

  private onNext() {
    this.$emit('on-next');
  }

  private onPrev() {
    this.$emit('on-prev');
  }

  get calcArticleIndex() {
    return this.articleIndex + ((this.page - 1) * 5) + 1;
  }

  get formatedDate() {
    return moment(this.data.created_at).format('DD MMMM YYYY, H:mm').toString();
  }

  get previewImage() {
    if (this.data.previews !== null && typeof this.data.previews !== 'undefined') {
      return 'https://vmap.atwinta.ru/' + this.data.previews['260x190'];
    }
    else {
      return '';
    }
  }
}
