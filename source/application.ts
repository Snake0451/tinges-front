import './stylesheets/main.scss';
import 'modernizr';

import Vue from 'vue';
import { Store } from 'vuex';
import VueRouter from 'vue-router';
import Vuebar from 'vuebar';
import { sync } from 'vuex-router-sync';

import TypedStore from './app/core/store/typedstore';
import ApplicationStore from './app/core/store';
import LayoutComponent from './app/layout';
import WidgetComponent from './app/widget';

import router from './app/core/router';

const getParameter = require('get-parameter');

Vue.use(VueRouter);
Vue.use(Vuebar);
// Vue.component('async-webpack-example', function (resolve) {
//   // Специальный синтаксис require укажет Webpack
//   // автоматически разделить сборку на части
//   // для последующей асинхронной загрузки
//   require(['vue-chartjs'], resolve)
// })

class Application {

  private vueInstance: Vue;

  private router: VueRouter;

  private store: ApplicationStore;

  private entryComponent: any = LayoutComponent;

  constructor(inRouter: VueRouter, inStore: ApplicationStore) {
    this.router = inRouter;

    this.store = inStore;

    sync(this.store, this.router);

    let params = getParameter();

    if (typeof params['lat'] !== 'undefined' && typeof params['lng'] !== 'undefined') {
      this.entryComponent = WidgetComponent;
    }

    this.init();
  }

  private init(): void {
    setTimeout(() => {
      this.vueInstance = new Vue({
        el: '#application',
        router: this.router,
        store: this.store,
        render: (createElement) => createElement(this.entryComponent)
      });
    }, 1000);
  }
}

new Application(router, new ApplicationStore());
