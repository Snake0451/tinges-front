/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	/*globals window __webpack_hash__ */
	if(false) {
		var lastHash;
		var upToDate = function upToDate() {
			return lastHash.indexOf(__webpack_hash__) >= 0;
		};
		var log = require("./log");
		var check = function check() {
			module.hot.check(true).then(function(updatedModules) {
				if(!updatedModules) {
					log("warning", "[HMR] Cannot find update. Need to do a full reload!");
					log("warning", "[HMR] (Probably because of restarting the webpack-dev-server)");
					window.location.reload();
					return;
				}

				if(!upToDate()) {
					check();
				}

				require("./log-apply-result")(updatedModules, updatedModules);

				if(upToDate()) {
					log("info", "[HMR] App is up to date.");
				}

			}).catch(function(err) {
				var status = module.hot.status();
				if(["abort", "fail"].indexOf(status) >= 0) {
					log("warning", "[HMR] Cannot apply update. Need to do a full reload!");
					log("warning", "[HMR] " + err.stack || err.message);
					window.location.reload();
				} else {
					log("warning", "[HMR] Update failed: " + err.stack || err.message);
				}
			});
		};
		var hotEmitter = require("./emitter");
		hotEmitter.on("webpackHotUpdate", function(currentHash) {
			lastHash = currentHash;
			if(!upToDate() && module.hot.status() === "idle") {
				log("info", "[HMR] Checking for updates on the server...");
				check();
			}
		});
		log("info", "[HMR] Waiting for update signal from WDS...");
	} else {
		throw new Error("[HMR] Hot Module Replacement is disabled.");
	}


/***/ })
/******/ ]);