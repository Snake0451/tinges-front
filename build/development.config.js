const webpackConfig = require("./common.config");
const DefinePlugin = require('webpack/lib/DefinePlugin');
const env = require('./env/development.env');
const helpers = require('./utils/helpers');
var path = require('path');

const publicPath = '/';

webpackConfig.output.publicPath = publicPath;

webpackConfig.devServer = {
  port: 3000,
  host: "localhost",
  historyApiFallback: true,
  watchOptions: {aggregateTimeout: 300, poll: 1000},
  contentBase: './dist/',
  open: true,
  openPage: ''
};

webpackConfig.module.rules = [...webpackConfig.module.rules, {
    test: /\.css$/,
    use: [
      {
        loader: 'style-loader',
        options: {
          insertAt: 'top'
        }
      },
      {
        loader: 'css-loader'
      }
    ]
  },{
    test: /\.scss$/,
    enforce: 'pre',
    use: [{
      loader: 'style-loader'
    },{
      loader: 'css-loader',
      options: {
        sourceMap:true
      }
    },{
      loader: 'autoprefixer-loader'
    },{
      loader: 'resolve-url-loader'
    },{
      loader: 'sass-loader',
      options: {
        sourceMap:true
      }
    }]
  },{
    test: /\.(png|jpg|gif|svg)$/i,
    use: [{
      loader: 'url-loader',
      options: {
        limit: 100,
        name: '[name].[hash:5].[ext]',
        outputPath: 'images/',
        publicPath: publicPath
      }
    }]
  },{
    test: /\.(eot|ttf|woff|woff2)$/,
    enforce: 'pre',
    use: [{
      loader: 'file-loader',
      options: {
        name: '[name].[hash].[ext]',
        outputPath: 'fonts/',
        publicPath: publicPath
      }
    }]
  }
]

webpackConfig.plugins = [...webpackConfig.plugins,
  new DefinePlugin({
    'process.env': env
  })
];

module.exports = webpackConfig;
