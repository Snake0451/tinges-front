const webpack = require('webpack');
const webpackConfig = require('./common.config');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const env = require('./env/development.env');

webpackConfig.module.rules = [{ 
    test: /\.pug$/, 
    enforce: 'pre', 
    loader: 'template-html-loader'
  },{ 
    test: /\.ts$/, 
    exclude: /node_modules/, 
    loader: 'awesome-typescript-loader',
    query: {
      compilerOptions: {
        inlineSourceMap: true,
        sourceMap: false
      }
    }    
  },{
    test: /\.scss$/,
    enforce: 'pre', 
    loader: 'style-loader!css-loader!resolve-url-loader!sass-loader?sourceMap'    
  },{
    test: /\.(?:png|jpg|svg)$/,
    loader: 'url-loader',
    options: {
      limit: 25000,
    },
  },{
    test: /\.(?:png|jpg|svg)$/,
    loader: 'file-loader',
    options: {
      name: '[path][name].[hash].[ext]',
    },
  },{
    test: /\.(eot|svg|ttf|woff|woff2)$/,
    loader: 'file?name=assets/fonts/[name].[ext]'
}];

webpackConfig.plugins = [...webpackConfig.plugins,
  new webpack.SourceMapDevToolPlugin({
    filename: null, // if no value is provided the sourcemap is inlined
    test: /\.(ts|js)($|\?)/i
  }),
  new DefinePlugin({
    'process.env': env
  })
];

webpackConfig.devtool = 'inline-source-map';

module.exports = webpackConfig;