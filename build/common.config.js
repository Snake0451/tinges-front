var webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const helpers = require('./utils/helpers');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ManifestPlugin = require('webpack-manifest-plugin');

module.exports = {
  entry: {
    "application": helpers.root('/source/application.ts')
  },
  output: {
    path: helpers.root('/dist'),
    filename: '[name].[hash].js'
  },
  resolve: {
    extensions: ['.ts', '.js', '.pug'],
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      'modernizr$': helpers.root('.modernizrrc')
    },
    modules: ['node_modules', helpers.root('/source')]
  },
  module: {
    rules: [{
        test: /\.pug$/,
        enforce: 'pre',
        loader: 'template-html-loader'
      },{
        loader: 'webpack-modernizr-loader?useConfigFile',
        test: /\.modernizrrc$/,
      },{
        test: /\.ts$/,
        exclude: /node_modules/,
        enforce: 'pre',
        loader: 'tslint-loader'
      },{
        test: /\.vue$/,
        loader: 'vue-loader',
      },{
        test: /\.ts$/,
        include: [
          helpers.root('/source'),
          helpers.root('/node_modules/vuex-ts-decorators')
        ],
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HtmlWebpackPlugin({
      filename: helpers.root('/dist/index.html'),
      template: helpers.root('/source/index.html')
    }),
    new ManifestPlugin()
  ]
}
