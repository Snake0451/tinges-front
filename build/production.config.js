const webpackConfig = require("./common.config");
const UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
const CompressionPlugin = require('compression-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const helpers = require('./utils/helpers');
const env = require('./env/production.env');
var path = require('path');

const publicPath = '/assets/';

webpackConfig.output.publicPath = publicPath;

webpackConfig.module.rules = [...webpackConfig.module.rules, {
    test: /\.scss$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [{
        loader: 'css-loader',
        options: {
          sourceMap:true
        }
      },{
        loader: 'autoprefixer-loader'
      },{
        loader: 'resolve-url-loader'
      },{
        loader: 'sass-loader',
        options: {
          sourceMap:true
        }
      }]
    })
  },{
    test: /\.(png|jpg|gif|svg)$/i,
    use: [{
      loader: 'url-loader',
      options: {
        limit: 100,
        name: '[name].[hash:5].[ext]',
        outputPath: 'images/',
        publicPath: publicPath
      }
    }]
  },{
    test: /\.(eot|ttf|woff|woff2)$/,
    enforce: 'pre',
    use: [{
      loader: 'file-loader',
      options: {
        name: '[name].[hash].[ext]',
        outputPath: 'fonts/',
        publicPath: publicPath
      }
    }]
  }
];

webpackConfig.plugins = [...webpackConfig.plugins,
  new DefinePlugin({
    'process.env': env
  }),
  new UglifyJsPlugin({
    include: /\.js$/,
    minimize: true
  }),
  new ExtractTextPlugin({
    filename: "application.[contenthash].css"
  }),
  new OptimizeCssAssetsPlugin({
    assetNameRegExp: /\.css$/g,
    cssProcessor: require('cssnano'),
    cssProcessorOptions: {
      discardComments: {removeAll: true }
      //discardUnused: {fontFace: false}
    },
    canPrint: true
  }),
  new CompressionPlugin({
    asset: "[path].gz[query]",
    algorithm: "gzip",
    test: /\.js$|\.css$/
  })
];

module.exports = webpackConfig;
