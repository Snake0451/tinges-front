# Varlamov Map

> A Vue 2.4 Webpack and TypeScript single page application starter-kit with hot reload, sass, pug, bundling and unit testing.

### Dependencies
---
- `pug v.2.0.0-rc.2`: A clean, whitespace-sensitive template language for writing HTML
- `vue v.2.4.1`: Reactive, component-oriented view layer for modern web interfaces
- `vue-class-component v.5.0.2`: ES201X/TypeScript class decorator for Vue components
- `vue-property-decorator v.5.1.1`: Property decorators for Vue Component
- `vue-resource v.1.3.4`: The HTTP client for Vue.js
- `vue-router v.2.7.0`: Official router for Vue.js 2
- `vuex v.2.3.1`: State management for Vue.js
- `vuex-class v.0.2.0`: Binding helpers for Vuex and vue-class-component
- `vuex-router-sync v.4.2.0`: Effortlessly keep vue-router and vuex store in sync

### Comands
---
- `npm run dev`: Starts webpacl server for development
- `npm run build`: Builds production version of the application
- `npm run clean`: Deletes 'dist' folder from project 

### Tests
---
- `npm run tests`: Running all test once
- `npm run tests:watch`: Running all test with watcher. Test runs each change in the code.
